import Vue from 'vue'

import VueTimepicker from 'vue2-timepicker'
import '~/assets/styles/timepicker.css'

Vue.component('vue-timepicker', VueTimepicker)
