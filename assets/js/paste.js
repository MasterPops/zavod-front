document.addEventListener("paste", function (event) {
  let target = event.target.closest('.js-split-clipbord input');
  if (target) {
    let paste = (event.clipboardData || window.clipboardData).getData('text/plain');
    let pasteString = paste.toString();
    let result = pasteString.split('\n').map(line => line.trim()).filter(line => line);
    let line = result[result.length - 1];
    let values = line.split('\t');
    if (values.length > 1) {
      document.querySelectorAll('.js-split-clipbord input').forEach((element, index) => {
        element.value = values[index] || '';
      });
    }
    event.preventDefault();
    return false;
  }
})
