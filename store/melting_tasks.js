export const state = () => ({
  overdue: [],
  working: [],
  planned: [],
  withoutDate: [],
  done: [],
  index: [],
  filters: [],
})

export const getters = {
  OVERDUE(state) {
    return state.overdue;
  },
  WORKING(state) {
    return state.overdue;
  },
  PLANNED(state) {
    return state.planned;
  },
  WITHOUT_DATE(state) {
    return state.withoutDate;
  },
  DONE(state) {
    return state.done;
  },
  INDEX(state) {
    return state.index;
  },
  filters(state) {
    return state.filters;
  },
}

export const actions = {
  GET_OVERDUE({commit}) {
    return this.$axios('/v1/melting-task/overdue', {
      method: 'GET'
    }).then((items) => {
      commit('SET_OVERDUE', items.data.data);
      return items;
    }).catch((error) => {
      console.log(error);
      return error
    })
  },
  GET_WORKING({commit}) {
    return this.$axios('/v1/melting-task/working', {
      method: 'GET'
    }).then((items) => {
      commit('SET_WORKING', items.data.data);
      return items;
    }).catch((error) => {
      console.log(error);
      return error
    })
  },
  GET_PLANNED({commit}) {
    return this.$axios('/v1/melting-task/planned', {
      method: 'GET'
    }).then((items) => {
      commit('SET_PLANNED', items.data.data);
      return items;
    }).catch((error) => {
      console.log(error);
      return error
    })
  },
  GET_WITHOUT_DATE({commit}) {
    return this.$axios('/v1/melting-task/without-date', {
      method: 'GET'
    }).then((items) => {
      commit('SET_WITHOUT_DATE', items.data.data);
      return items;
    }).catch((error) => {
      console.log(error);
      return error
    })
  },
  GET_DONE({commit}) {
    return this.$axios('/v1/melting-task/done', {
      method: 'GET'
    }).then((items) => {
      commit('SET_DONE', items.data);
      return items;
    }).catch((error) => {
      console.log(error);
      return error
    })
  },
  GET_INDEX({commit}) {
    return this.$axios('/v1/melting-task', {
      method: 'GET'
    }).then((items) => {
      commit('SET_INDEX', items.data.data);
      return items;
    }).catch((error) => {
      console.log(error);
      return error
    })
  },
  GET_FILTERS({commit}) {
    return this.$axios('/v1/melting-task/filters', {
      method: 'GET'
    }).then((items) => {
      commit('SET_FILTERS', items.data);
      return items;
    }).catch((error) => {
      console.log(error);
      return error
    })
  },
}

export const mutations = {
  SET_OVERDUE: (state, items) => {
    state.overdue = items;
  },
  SET_WORKING: (state, items) => {
    state.working = items;
  },
  SET_PLANNED: (state, items) => {
    state.planned = items;
  },
  SET_WITHOUT_DATE: (state, items) => {
    state.withoutDate = items;
  },
  SET_DONE: (state, items) => {
    state.done = items;
  },
  SET_INDEX: (state, items) => {
    state.index = items;
  },
  SET_FILTERS: (state, items) => {
    state.filters = items;
  },

}

