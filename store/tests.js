export const state = () => ({
  tests: [],
})

export const getters = {
  TESTS(state) {
    return state.tests;
  },
}

export const actions = {
  GET_TESTS({commit}) {
    return this.$axios('/v1/test', {
      method: 'GET'
    }).then((items) => {
      commit('SET_TESTS', items.data.data);
      return items;
    }).catch((error) => {
      console.log(error);
      return error
    })
  },
}

export const mutations = {
  SET_TESTS: (state, items) => {
    state.tests = items;
  },
}

